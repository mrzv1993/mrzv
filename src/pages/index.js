import React from "react";
import "reset-css";
import Header from "../components/Header";
import Hero from "../components/Hero";
import { graphql, Link } from "gatsby";
import styled from "styled-components";
import Moment from "react-moment";
import "moment/locale/ru";
import Helmet from "react-helmet";

const Page = styled.main`
  min-width: 320px;
  font-family: "Source Sans Pro", sans-serif;
  font-weight: 900;
`;

const Container = styled.main`
  max-width: 940px;
  margin: 0 auto;
  padding: 0 16px;
`;

const BlogTitle = styled.div`
  font-family: "Source Sans Pro", sans-serif;
  font-weight: 900;
  font-size: 32px;
  color: #2c3949;
  margin-top: 64px;

  p {
    font-family: "Fira Mono", monospace;
    font-weight: 400;
    font-size: 15px;
    color: #6c7480;
    margin-top: 8px;
  }
`;

const Post = styled.div`
  display: flex;
  align-items: baseline;
  margin-top: 24px;
  max-width: 940px;
  margin: 0 auto;
`;

const Time = styled(Moment)`
  font-family: "Fira Mono", monospace;
  font-weight: 400;
  color: #6c7480;
  font-size: 14px;
  margin-right: 24px;
`;

const PostTitle = styled(Link)`
  font-size: 21px;
  color: #2c3949;
  font-weight: 700;
  margin-top: 8px;
  text-decoration: none;
  transition: all 0.24s;
  margin-top: 32px;

  &:hover {
    color: #428dff;
  }
`;

const Layout = ({ data, pageContext }) => {
  const { edges: posts } = data.allMarkdownRemark;
  return (
    <Page>
      <Helmet title="Станислав Морозов — Фронтенд дизайнер. Главная страница" />
      <Header />
      <Container>
        <Hero />
        <BlogTitle>
          Записи
          <br />
          <p>
            Пишу о дизайне интерфейсов, фронтенде, личном опыте и удаленной
            работе.
          </p>
        </BlogTitle>
        {posts.map(({ node: post }, index) => {
          const { frontmatter } = post;
          return (
            <Post key={frontmatter.path}>
              <Time format="D MMM YYYY" withTitle locale="ru">
                {frontmatter.date}
              </Time>
              <PostTitle to={frontmatter.path}>{frontmatter.title}</PostTitle>
            </Post>
          );
        })}
      </Container>
    </Page>
  );
};

export const query = graphql`
  query HomepageQuery {
    allMarkdownRemark(sort: { order: DESC, fields: [frontmatter___date] }) {
      edges {
        node {
          id
          frontmatter {
            title
            path
            date
            tags
            excerpt
          }
        }
      }
    }
  }
`;

export default Layout;
