import React from "react";
import styled from "styled-components";
import { Link } from "gatsby";
import logo from "./logo.png";

const Head = styled.header`
  padding: 16px 0;
  border-bottom: 1px solid #eceef0;
`;

const HeaderWrap = styled.header`
  max-width: 940px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 0 auto;
`;

const Logo = styled(Link)`
  width: 40px;
  height: 40px;
  background-color: #4a4fd9;
  background-image: url(${logo});
  background-repeat: no-repeat;
  background-position: center;
  background-size: 50%;
  border-radius: 50%;
  margin-right: 16px;
`;

const Nav = styled.nav`
  display: flex;
  justify-content: flex-start;
`;

const DribbbleLink = styled.a`
  display: inline-block;
  text-decoration: none;
  margin-left: 12px;
  font-family: "Fira Mono", monospace;
  font-weight: 400;
  font-size: 15px;
  color: #6c7480;
  &:hover {
    color: #ea4c89;
  }
`;

const VkontakteLink = styled(DribbbleLink)`
  &:hover {
    color: #3d76b8;
  }
`;

export default function() {
  return (
    <Head>
      <HeaderWrap>
        <Logo to="/" />
        <Nav>
          <DribbbleLink href="https://dribbble.com/mrzv1993">
            Дрибббл
          </DribbbleLink>
          <VkontakteLink href="https://vk.com/mrzv1993">
            Вконтакте
          </VkontakteLink>
        </Nav>
      </HeaderWrap>
    </Head>
  );
}
