import React from "react";
import styled from "styled-components";
import me from "./me.svg";

const Hero = styled.section`
  font-size: 40px;
  color: #2c3949;
  line-height: 1.2;
  margin-top: 64px;
`;

const FrontDesign = styled.p`
  position: relative;
`;

const Img = styled.img`
  position: absolute;
  top: 4px;
`;

export default function() {
  return (
    <Hero>
      Привет, <br />
      меня зовут Станислав Морозов🖖🏼 <br />
      <FrontDesign>
        Я фронтенд дизайнер&nbsp;
        <Img src={me} alt="" />
      </FrontDesign>
    </Hero>
  );
}
