import React from "react";
import { Link } from "gatsby";

const AllTagsIndex = ({ data, pageContext }) => {
  const { tags } = pageContext;
  return (
    <div>
      <ul>
        <li>Все тэги</li>
        {tags.map((tagName, index) => {
          return (
            <li key={index}>
              <Link to={`tags/${tagName}`}>{tagName}</Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default AllTagsIndex;
