import React from "react";
import { graphql, Link } from "gatsby";
import Header from "../components/Header";
import styled from "styled-components";
import Moment from "react-moment";
import "moment/locale/ru";
import { Facebook, Twitter, VK, Telegram } from "react-social-sharing";
import Helmet from "react-helmet";

const Page = styled.main`
  min-width: 320px;
  font-family: "Fira Mono", monospace;
  font-weight: 400;
  font-size: 17px;
  line-height: 30px;
  margin: 0 auto;
`;

const Container = styled.main`
  max-width: 940px;
  margin: 0 auto;
  padding: 0 16px;
`;

const Intro = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
`;

const Title = styled.h1`
  font-size: 40px;
  text-align: center;
  font-family: "Source Sans Pro", sans-serif;
  font-weight: 700;
  margin-top: 32px;
  color: #354356;
`;

const Time = styled(Moment)`
  color: #849099;
  font-size: 11px;
  margin-top: 96px;
`;

const Tags = styled.ul`
  display: flex;
  margin-top: 32px;
  list-style-type: none;
`;

const Tag = styled.li`
  padding-right: 8px;
`;

const BlogPost = styled.div`
  margin-top: 32px;
  h2,
  h3,
  h4,
  h5,
  h6,
  p {
    max-width: 640px;
    margin: 0 auto;
    color: #354356;
  }
`;

const Share = styled.div``;

const Author = styled.div`
  margin: 32px 0;
  padding: 16px 24px;
  background-color: #f1f1f1;
  font-size: 15px;
  border-radius: 4px;
`;

const Template = ({ data, pageContext }) => {
  const { next, prev } = pageContext;
  const { markdownRemark } = data;
  const title = markdownRemark.frontmatter.title;
  const date = markdownRemark.frontmatter.date;
  const tags = markdownRemark.frontmatter.tags;
  const html = markdownRemark.html;
  return (
    <Page>
      <Helmet title={markdownRemark.frontmatter.title} />
      <Header />
      <Container>
        <Intro>
          <Time format="D MMM YYYY" withTitle locale="ru">
            {date}
          </Time>
          <Title>{title}</Title>
          <Tags>
            {tags.map((tagName, index) => {
              return (
                <Tag key={index}>
                  <Link to={`tags/${tagName}`}>{tagName}</Link>
                </Tag>
              );
            })}
          </Tags>
        </Intro>
        <BlogPost dangerouslySetInnerHTML={{ __html: html }} />
        <Share>
          <Facebook solidcircle medium link={markdownRemark.frontmatter.path} />
          <Twitter solidcircle medium link={markdownRemark.frontmatter.path} />
          <VK solidcircle medium link={markdownRemark.frontmatter.path} />
          <VK solidcircle medium link="https://dribbble.com" />
          <Telegram solidcircle medium link={markdownRemark.frontmatter.path} />
        </Share>
        <Author>
          Станислав Морозов — фронтенд дизайнер. Добавляйтесь ко мне в друзья во
          вконтакте, если хотите получать информацию о новых постах. Я открыт
          для ваших предложений и работы. Не стесняйтесь писать мне по любым
          вопросам.
        </Author>
        <div>
          {next && <Link to={next.frontmatter.path}>Следующий пост</Link>}
        </div>
        <div>
          {prev && <Link to={prev.frontmatter.path}>Предыдущий пост</Link>}
        </div>
      </Container>
    </Page>
  );
};

export const query = graphql`
  query($pathSlug: String!) {
    markdownRemark(frontmatter: { path: { eq: $pathSlug } }) {
      html
      frontmatter {
        title
        date
        tags
        path
      }
    }
  }
`;

export default Template;
