import React from "react";
import { graphql, Link } from "gatsby";
import styled from "styled-components";
import Moment from "react-moment";
import "moment/locale/ru";

const Post = styled.div`
  display: flex;
  flex-flow: column;
  margin-top: 24px;
`;

const Time = styled(Moment)`
  text-transform: uppercase;
  color: #849099;
  font-size: 11px;
`;

const PostTitle = styled(Link)`
  font-size: 18px;
  color: #354356;
  font-weight: 700;
  margin-top: 8px;
  text-decoration: none;
  transition: all 0.24s;

  &:hover {
    color: #428dff;
  }
`;

const SingleTagTemplate = ({ data, pageContext }) => {
  const { posts, tagName } = pageContext;
  console.log(pageContext);
  return (
    <div>
      <div>Посты по тэгу {tagName}</div>
      <div>
        {posts.map((post, index) => {
          return (
            <Post key={index}>
              <Time format="D MMM YYYY" withTitle locale="ru">
                {post.frontmatter.date}
              </Time>
              <PostTitle to={post.frontmatter.path}>
                {post.frontmatter.title}
              </PostTitle>
            </Post>
          );
        })}
      </div>
      <Link to={"/"}>На главную</Link>
    </div>
  );
};

export default SingleTagTemplate;
