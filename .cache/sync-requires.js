// prefer default export if available
const preferDefault = m => m && m.default || m


exports.components = {
  "component---src-templates-all-tags-index-js": preferDefault(require("/Users/stanislavmorozov/Desktop/mrzv/src/templates/allTagsIndex.js")),
  "component---src-templates-single-tag-index-js": preferDefault(require("/Users/stanislavmorozov/Desktop/mrzv/src/templates/singleTagIndex.js")),
  "component---src-templates-blog-post-js": preferDefault(require("/Users/stanislavmorozov/Desktop/mrzv/src/templates/blogPost.js")),
  "component---cache-dev-404-page-js": preferDefault(require("/Users/stanislavmorozov/Desktop/mrzv/.cache/dev-404-page.js")),
  "component---src-pages-index-js": preferDefault(require("/Users/stanislavmorozov/Desktop/mrzv/src/pages/index.js"))
}

