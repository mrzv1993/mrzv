// prefer default export if available
const preferDefault = m => m && m.default || m

exports.components = {
  "component---src-templates-all-tags-index-js": () => import("/Users/stanislavmorozov/Desktop/mrzv/src/templates/allTagsIndex.js" /* webpackChunkName: "component---src-templates-all-tags-index-js" */),
  "component---src-templates-single-tag-index-js": () => import("/Users/stanislavmorozov/Desktop/mrzv/src/templates/singleTagIndex.js" /* webpackChunkName: "component---src-templates-single-tag-index-js" */),
  "component---src-templates-blog-post-js": () => import("/Users/stanislavmorozov/Desktop/mrzv/src/templates/blogPost.js" /* webpackChunkName: "component---src-templates-blog-post-js" */),
  "component---cache-dev-404-page-js": () => import("/Users/stanislavmorozov/Desktop/mrzv/.cache/dev-404-page.js" /* webpackChunkName: "component---cache-dev-404-page-js" */),
  "component---src-pages-index-js": () => import("/Users/stanislavmorozov/Desktop/mrzv/src/pages/index.js" /* webpackChunkName: "component---src-pages-index-js" */)
}

exports.data = () => import("/Users/stanislavmorozov/Desktop/mrzv/.cache/data.json")

